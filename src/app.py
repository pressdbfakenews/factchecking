# Import Packages
## Standard Packages
import re
import socket
import functools
import time
from io import StringIO
from urllib.parse import urlparse
from collections import Counter
from concurrent.futures import ThreadPoolExecutor

## Installed Packages
import pandas as pd
import numpy as np
import dash
from dash import dcc
from dash import html
from dash import dash_table
from dash.dependencies import Input, Output, State

# from envparse import env
from flask import Flask
from flask_restful import reqparse, Resource, Api
import requests
import tldextract
from SPARQLWrapper import SPARQLWrapper, CSV, POST, POSTDIRECTLY
from requests import RequestException
from bs4 import BeautifulSoup

## Custom Libraries
import src.blocklists as bl
import src.media_analysis as ma
import src.domain_registration as dr
import src.web_infra as wi
import src.outlet_attributes as oa
import src.web_ranking as wr
import src.web_links as wl
import src.fact_checks as fc
import src.info_collection as ic


# Custom Functions
def create_table(df, style_data_conditional=None):
    """
    Convert Pandas DataFrame into a Dash DataTable
        Parameters:
            df (pd.DataFrame): Input DataFrame
            style_data_conditional (list): CSS conditional formatting; Same argument as `style_data_conditional` for dash_table.DataTable
        Returns:
            (str): Website domain
    """
    if style_data_conditional is not None:
        tbl = dash_table.DataTable(
            id="table",
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict("records"),
            style_cell=dict(textAlign="left"),
            fill_width=False,
            style_data=dict(whiteSpace="normal", height="auto"),
            style_table={"overflowX": "auto"},
            style_data_conditional=style_data_conditional,
        )
    else:
        tbl = dash_table.DataTable(
            id="table",
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict("records"),
            style_cell=dict(textAlign="left"),
            fill_width=False,
            style_header=dict(backgroundColor="paleturquoise"),
            style_data=dict(
                whiteSpace="normal", height="auto", backgroundColor="lavender"
            ),
            style_table={"overflowX": "auto"},
        )
    return tbl


def get_sparql_query_results(endpoint_url, query, wikidata_flag=False):
    """
    Query a SPARQL endpoint and save results to a data frame
        Parameters:
            endpoint_url (str): SPARQL endpoint URL
            query (str): SPARQL query
            wikidata_flag (str): Whether or not the SPARQL endpoint queried is for WikiData
        Returns:
            (pd.DataFrame): DataFrame of SPARQL query results
    """
    sparql = SPARQLWrapper(endpoint_url)
    sparql.setQuery(query)
    sparql.setReturnFormat(CSV)
    if wikidata_flag:
        sparql.setOnlyConneg(True)
        sparql.addCustomHttpHeader("Content-type", "application/sparql-query")
        sparql.addCustomHttpHeader("Accept", "text/csv")
        sparql.setMethod(POST)
        sparql.setRequestMethod(POSTDIRECTLY)
    results = sparql.query().convert()
    results = results.decode("utf-8")
    df = pd.read_csv(StringIO(results))
    return df


# Dash
server = Flask("app")
title = "Veri.FYI: Web Domain Risk Analysis"
app = dash.Dash(__name__, server=server, title=title)

# API
api = Api(server)
parser = reqparse.RequestParser()
parser.add_argument("url")


class VeriFYI(Resource):
    """
    API endpoint for Veri.FYI
    """

    def post(self):
        """
        Given a URL, count the number of times that its domain appears in unreliable sources lists or spam blocklists
        """
        args = parser.parse_args()
        if args["url"] is None or args["url"].strip() == "":
            return {
                "Error": "Domain cannot be extracted. Please try again with another URL"
            }, 400
        url = args["url"].lower()
        domain = ic.get_domain(url=url)
        unreliable_blocklists = [
            ("OpenSources.co", bl.OpenSources),
            ("List of Pink Slime Sites from Columbia Journalism Review", bl.PinkSlime),
            ("iffy.news", bl.IffyNews),
            (
                "Wikipedia English - Lists of Fake News Websites",
                bl.WikipediaEnglishFakeNews,
            ),
            (
                "Wikipedia English - Lists of Satirical News Websites",
                bl.WikipediaEnglishSatiricalNews,
            ),
            ("Wikipedia Czech - List of Fake News Websites", bl.WikipediaCzechFakeNews),
        ]
        n_unreliable_blocklists = len(unreliable_blocklists)
        unreliable_blocklist_hits = []
        n_unreliable_blocklist_hits = 0
        for name, blocklist in unreliable_blocklists:
            blocklist_hit = blocklist(domain).query_blocklist()
            n_unreliable_blocklist_hits += blocklist_hit
            if blocklist_hit:
                unreliable_blocklist_hits.append(name)

        spam_blocklists = [
            (
                "Wikimedia Global Spam Blacklist",
                bl.WikimediaGlobalSpamBlocklist(domain).query_blocklist(
                    bl.wikimedia_bl_url
                ),
            ),
            (
                "English Wikipedia Local Spam Blacklist",
                bl.WikipediaEnglishSpamBlocklist(domain).query_blocklist(
                    bl.en_wikipedia_bl_url
                ),
            ),
        ]
        n_spam_blocklists = len(spam_blocklists)
        spam_blocklist_hits = []
        n_spam_blocklist_hits = 0
        for name, blocklist in spam_blocklists:
            n_spam_blocklist_hits += blocklist
            if blocklist:
                spam_blocklist_hits.append(name)
        return {
            "Total Unreliable Source Blocklist Hits": n_unreliable_blocklist_hits,
            "Unreliable Source Blocklist Hits": unreliable_blocklist_hits,
            "Total Spam Blocklist Hits": n_spam_blocklist_hits,
            "Spam Blocklist Hits": spam_blocklist_hits,
        }, 200


api.add_resource(VeriFYI, "/assess")

# HTML/CSS
## Note that much of this CSS comes from https://codepen.io/chriddyp/pen/bWLwgP.css
logo_image_style = {
    "textAlign": "left",
    "display": "block",
    "margin-left": "auto",
    "margin-right": "auto",
    "padding-top": 20,
    "height": "10%",
    "width": "10%",
}
input_style = {
    "height": "38px",
    "padding": "6px 10px",
    "background-color": "#fff",
    "border": "1px solid #D1D1D1",
    "border-radius": "4px",
    "box-shadow": "none",
    "box-sizing": "border-box",
    "width": "900px",
}
button_style = {
    "display": "inline-block",
    "height": "38px",
    "padding": "0 30px",
    "color": "#555",
    "text-align": "center",
    "font-weight": "600",
    "line-height": "38px",
    "letter-spacing": ".1rem",
    "text-decoration": "none",
    "white-space": "nowrap",
    "background-color": "transparent",
    "border-radius": "4px",
    "border": "1px solid #bbb",
    "cursor": "pointer",
    "box-sizing": "border-box",
}
div_style = {
    "font-size": "1em",
    "line-height": "0px",
    "margin": "3",
    "margin-bottom": "3rem",
    "position": "relative",
    "top": "3rem",
    "left": "0",
    "textAlign": "left",
    "padding-top": "1rem",
    "padding-left": "5rem",
}

details_style = {"margin-left": "40px"}

app.layout = html.Article(
    [
        html.Img(src=app.get_asset_url("PressDB_Logo.png"), style=logo_image_style),
        html.H1(title),
        html.P(
            "Assess websites for misinformation risk based on publicly available data."
        ),
        html.P(
            "Veri.FYI takes a URL or domain (ex. 'https://example.com' or 'example.com') as an input and outputs indicators that can be used to assess the source's reliability."
        ),
        html.P(
            "Are you a journalist, fact-checker or researcher? Does this service give you the information you need to accurately assess the credibility of a website? We'd love to get your feedback on how we can "
            "better serve you."
        ),
        html.P(
            "Try our Veri.FYI prototype and send any questions and comments to partners \[at\] pressdb \["
            "dot\] info."
        ),
        dcc.Input(
            id="article_url",
            type="url",
            placeholder="Enter URL to review here. For example: https://example.com",
            style=input_style,
        ),
        html.Br(),
        html.Button(
            id="submit-button", n_clicks=0, children="Submit", style=button_style
        ),
        html.Button(
            id="reset-button", n_clicks=0, children="Reset", style=button_style
        ),
        html.Br(),
        dcc.Loading(
            id="loading-basic-info",
            type="circle",
            children=html.Section(id="basic-info-output"),
        ),
        dcc.Loading(
            id="loading-blocklist-output",
            type="circle",
            children=html.Section(id="blocklist-output"),
        ),
        dcc.Loading(
            id="loading-web-infra-output",
            type="circle",
            children=html.Section(id="web-infra-output"),
        ),
        dcc.Loading(
            id="loading-media-assess-output",
            type="circle",
            children=html.Section(id="media-assess-output"),
        ),
        dcc.Loading(
            id="loading-domain-registration-output",
            type="circle",
            children=html.Section(id="domain-registration-output"),
        ),
        dcc.Loading(
            id="loading-web-rank-output",
            type="circle",
            children=html.Section(id="web-rank-output"),
        ),
        dcc.Loading(
            id="loading-web-link-output",
            type="circle",
            children=html.Section(id="web-link-output"),
        ),
        dcc.Loading(
            id="loading-wikidata-output",
            type="circle",
            children=html.Section(id="wikidata-output"),
        ),
        dcc.Loading(
            id="loading-factcheck-output",
            type="circle",
            children=html.Section(id="factcheck-output"),
        ),
        html.Br(),
        "This work is licensed under a ",
        html.A(
            "GNU General Public License, version 3.0",
            href="https://www.gnu.org/licenses/gpl-3.0.en.html",
            target="_top",
        ),
        ".",
        html.Br(),
        html.A(
            "GitLab project link",
            href="https://gitlab.com/pressdbfakenews/factchecking",
            target="_top",
        ),
    ]
)


error_layout = [
    "Error: Domain cannot be extracted. Please try again with another " "URL."
]


@app.callback(
    Output("basic-info-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_basic_info(n_clicks, value):
    """
    Display basic information about website (URL, domain, IP address)
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = error_layout
        else:
            # Basic Information
            url_sanitized = ic.sanitize_url(value)
            domain_sanitized = ic.sanitize_url(domain)
            ip_address = ic.query_ip_address(domain)

            submit_layout = [
                html.H2("Source"),
                html.H3("Basic Information"),
                html.Ul(
                    [
                        html.Li([html.Em("URL:"), " ", url_sanitized]),
                        html.Li([html.Em("Website Domain:"), " ", domain_sanitized]),
                        html.Li([html.Em("IP Address:"), " ", ip_address]),
                    ]
                ),
            ]
        return submit_layout


@app.callback(
    Output("blocklist-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_blocklists(n_clicks, value):
    """
    Display blocklist checks (unreliable sources, spam) for a given website
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Unreliable Source Checks / Spam Blocklist Checks

            def blocklist_wrapper(blocklist, domain):
                bl_obj = blocklist(domain)
                in_bl = bl_obj.query_blocklist()
                bl_layout = bl_obj.get_app_layout()
                return {"in_blocklist": in_bl, "submit_layout": bl_layout}

            def wp_spam_blocklist_wrapper(blocklist, domain, url):
                bl_obj = blocklist(domain)
                in_bl = bl_obj.query_blocklist(url)
                bl_layout = bl_obj.get_app_layout()
                return {"in_blocklist": in_bl, "submit_layout": bl_layout}

            unreliable_blocklists = [
                ("OpenSources.co", bl.OpenSources),
                (
                    "List of Pink Slime Sites from Columbia Journalism Review",
                    bl.PinkSlime,
                ),
                ("iffy.news", bl.IffyNews),
                (
                    "Wikipedia English - Lists of Fake News Websites",
                    bl.WikipediaEnglishFakeNews,
                ),
                (
                    "Wikipedia English - Lists of Satirical News Websites",
                    bl.WikipediaEnglishSatiricalNews,
                ),
                (
                    "Wikipedia Czech - List of Fake News Websites",
                    bl.WikipediaCzechFakeNews,
                ),
            ]

            spam_blocklists = [
                (
                    "Wikimedia Global Spam Blacklist",
                    bl.WikimediaGlobalSpamBlocklist,
                    bl.wikimedia_bl_url,
                ),
                (
                    "English Wikipedia Local Spam Blacklist",
                    bl.WikipediaEnglishSpamBlocklist,
                    bl.en_wikipedia_bl_url,
                ),
            ]

            with ThreadPoolExecutor() as executor:
                running_tasks = [
                    (name, executor.submit(blocklist_wrapper, blocklist, domain))
                    for (name, blocklist) in unreliable_blocklists
                ]
                running_tasks_spam = [
                    (
                        name,
                        executor.submit(
                            wp_spam_blocklist_wrapper, blocklist, domain, url
                        ),
                    )
                    for (name, blocklist, url) in spam_blocklists
                ]
                blocklist_map = {}
                for running_task in running_tasks:
                    blocklist_map[running_task[0]] = running_task[1].result()
                for running_task in running_tasks_spam:
                    blocklist_map[running_task[0]] = running_task[1].result()

            unreliable_layout = (
                blocklist_map["OpenSources.co"]["submit_layout"]
                + blocklist_map[
                    "List of Pink Slime Sites from Columbia Journalism Review"
                ]["submit_layout"]
                + blocklist_map["iffy.news"]["submit_layout"]
                + blocklist_map["Wikipedia English - Lists of Fake News Websites"][
                    "submit_layout"
                ]
                + blocklist_map["Wikipedia English - Lists of Satirical News Websites"][
                    "submit_layout"
                ]
                + blocklist_map["Wikipedia Czech - List of Fake News Websites"][
                    "submit_layout"
                ]
            )

            spam_layout = (
                blocklist_map["Wikimedia Global Spam Blacklist"]["submit_layout"]
                + blocklist_map["English Wikipedia Local Spam Blacklist"][
                    "submit_layout"
                ]
            )

            # Counts the number of blocklists that contain a domain
            total_blocklist_hits = bl.n_blocklist_hits(
                blocklist_map["OpenSources.co"]["in_blocklist"],
                blocklist_map[
                    "List of Pink Slime Sites from Columbia Journalism Review"
                ]["in_blocklist"],
                blocklist_map["iffy.news"]["in_blocklist"],
                blocklist_map["Wikipedia English - Lists of Fake News Websites"][
                    "in_blocklist"
                ],
                blocklist_map["Wikipedia English - Lists of Satirical News Websites"][
                    "in_blocklist"
                ],
                blocklist_map["Wikipedia Czech - List of Fake News Websites"][
                    "in_blocklist"
                ],
                blocklist_map["Wikimedia Global Spam Blacklist"]["in_blocklist"],
                blocklist_map["English Wikipedia Local Spam Blacklist"]["in_blocklist"],
            )

            submit_layout = [
                html.H2(f"Total Blocklist Hits: {total_blocklist_hits}"),
                html.Details(
                    children=[
                        html.Summary(html.A("More Info")),
                        "Counts the number of unreliable sources lists or spam blocklists that contain the given site.",
                    ],
                    style=details_style,
                ),
                html.H3("Unreliable Source Checks"),
                html.Ul(unreliable_layout),
                html.H3("Spam Blocklist Checks"),
                html.Ul(spam_layout),
            ]
        return submit_layout


@app.callback(
    Output("web-infra-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_web_infra(n_clicks, value):
    """
    Display information about website infrastructure (trust.txt, ads.txt)
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Website Infrastructure
            trusttxt = wi.TrustTxt(domain=domain)
            _ = trusttxt.query_trust()
            trust_layout = trusttxt.get_app_layout()

            adstxt = wi.AdsTxt(domain=domain)
            _ = adstxt.ads_txt_check()
            ads_layout = adstxt.get_app_layout()

            submit_layout = [
                html.H3("Website Infrastructure"),
                html.Ul(trust_layout + ads_layout),
            ]
        return submit_layout


@app.callback(
    Output("media-assess-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_media_assessments(n_clicks, value):
    """
    Dislay media reliability assessments (Media Bias/Fact Check, English Wikipedia Perennial Sources List, Le Monde's Décodex) about website
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:

            # Media Reliability Assessments

            ## Media Bias/Fact-Check
            mbfc = ma.MBFC(domain)
            in_mbfc, mbfc_data = mbfc.query_media_list()
            mbfc_layout = mbfc.get_app_layout()

            ## Wikipedia English Perennial Sources
            per = ma.WikipediaEnglishPerennial(domain)
            in_per, per_data = per.query_media_list()
            per_layout = per.get_app_layout()

            ## Decodex
            decodex = ma.Decodex(domain)
            in_decodex, decodex_data = decodex.query_media_list()
            decodex_layout = decodex.get_app_layout()

            ## Counts the number of media reliability assessments that rate a domain as unreliable
            media_reliability_red_flags = ma.n_media_reliability_red_flags(
                mbfc_data, per_data, decodex_data
            )

            submit_layout = [
                html.H2(f"Total Media Reliability Hits: {media_reliability_red_flags}"),
                html.Details(
                    children=[
                        html.Summary(html.A("More Info")),
                        "Counts the number of media reliability databases that rate the given site as unreliable.",
                    ],
                    style=details_style,
                ),
                html.H3("Media Reliability Assessments"),
                html.Ul(mbfc_layout + per_layout + decodex_layout),
            ]
        return submit_layout


@app.callback(
    Output("domain-registration-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_domain_registration(n_clicks, value):
    """
    Display domain registration information (WHOIS, RDAP) about website
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Domain Registration Information
            ip_address = ic.query_ip_address(domain)
            # TODO: How to reduce redundant computations/validations
            whois_layout = dr.query_whois(ip_address)
            rdap_layout = dr.extract_rdap_metadata(domain)

            submit_layout = [html.H2("Domain Registration Information")]
            submit_layout += whois_layout
            submit_layout += rdap_layout
        return submit_layout


@app.callback(
    Output("web-rank-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_web_ranking(n_clicks, value):
    """
    Display website ranking information (Tranco Rank)
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Website Ranking Information
            tranco_layout = wr.get_tranco_rank(domain)

            submit_layout = [html.H2("Website Ranking Information")]
            submit_layout += tranco_layout
        return submit_layout


@app.callback(
    Output("web-link-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_web_links(n_clicks, value):
    """
    Display external web links for a given URL
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Website Links
            outbound_layout = wl.get_outbound_domains(url=value, domain=domain)
            submit_layout = [html.H2("Website Links")]
            submit_layout += outbound_layout
        return submit_layout


@app.callback(
    Output("wikidata-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_wiki_attributes(n_clicks, value):
    """
    Display attributes about website from WikiData
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Outlet Attributes
            wikidata_layout = oa.query_wikidata(domain)
            submit_layout = [html.H2("Outlet Attributes")]
            submit_layout += wikidata_layout
        return submit_layout


@app.callback(
    Output("factcheck-output", "children"),
    [Input("submit-button", "n_clicks")],
    [State("article_url", "value")],
)
def get_factchecks(n_clicks, value):
    """
    Display most recent fact-checks (ClaimsKG) related to website
    """
    if n_clicks >= 1 and value.strip() not in ["", None]:
        value, domain = ic.input_validation(value)
        if domain in [None, ""]:
            submit_layout = None
        else:
            # Associated Fact-Checks
            claims_layout = fc.query_claimskg(domain)

            submit_layout = [html.H2("Associated Fact-Checks")]
            submit_layout += claims_layout
        return submit_layout


# Reset user input
@app.callback(Output("article_url", "value"), [Input("reset-button", "n_clicks")])
def update(reset):
    """
    Reset user input (article URL)
    """
    return ""


if __name__ == "__main__":
    app.run_server(debug=False, host="0.0.0.0")
