# Import Packages
## Standard Packages
import re
import functools

## Installed Packages
import pandas as pd
import requests
from bs4 import BeautifulSoup
from dash import dcc
from dash import html

## Custom Libraries
import src.app as app


class MediaReliabilityCheck:
    def __init__(self, domain):
        """
        Parameters:
            domain (str): Website domain
        """
        self.domain = domain
        self.in_media_list = None


class MBFC(MediaReliabilityCheck):
    def query_media_list(self):
        """
        Short description: Query whether a given domain is included in the database of Media Bias/Fact-Check (MBFC)

        Long description: Media Bias/Fact Check is arguably one of the most extensive publicly available databases of media websites and their political slant/reliability, and has also been cited by academic research (https://doi.org/10.1093/pnasnexus/pgad286). More details on its methodology are available on its official website (https://mediabiasfactcheck.com/methodology/). This list is queried in real time.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool, dict): Boolean assessment for whether domain is included; Bias, Credibility, and Reporting ratings from MBFC (if applicable)
        """
        df = pd.read_json(
            "https://raw.githubusercontent.com/drmikecrowe/mbfcext/main/docs/v5/data/sources.json"
        )
        self.media_ratings = {}
        for key in ["bias", "credibility", "name", "reporting"]:
            self.media_ratings[key] = "Not available"
        self.in_media_list = set([self.domain]).issubset(df["domain"])
        if self.in_media_list:
            idx = df[df["domain"] == self.domain].index
            for key in self.media_ratings.keys():
                try:
                    self.media_ratings[key] = df.loc[idx, key].values[0]
                except:
                    continue
        return self.in_media_list, self.media_ratings

    def mbfc_credibility_interface(self):
        """
        Color-coded output for credibility label from Media Bias/Fact Check
            Parameters:
                check (str): Credibility label from Media Bias/Fact Check
            Returns:
                (dash_html_component): Color-coded output, wrapped in Dash HTML components
        """
        color_map = {
            "high-credibility": "#008000",
            "low-credibility": "#ff0706",
            "medium-credibility": "#7B8705",
        }
        check = self.media_ratings["credibility"]
        if check is None or check in ["Not available", "n/a"]:
            return html.A(check)
        else:
            return html.Strong(
                children=html.A(check), style={"color": color_map[check]}
            )

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.media_list_interface = "Yes" if self.in_media_list is True else "No"
        self.submit_layout = [
            html.Li(
                [
                    html.Strong(
                        [
                            "Is domain in ",
                            html.A(
                                "Media Bias/Fact Check (MBFC)",
                                href="https://mediabiasfactcheck.com/",
                                target="_top",
                            ),
                            "?: ",
                        ]
                    ),
                    self.media_list_interface,
                    html.Ul(
                        [
                            html.Li(
                                [
                                    html.Strong(["MBFC Bias rating:"]),
                                    " ",
                                    self.media_ratings["bias"],
                                ]
                            ),
                            html.Li(
                                [
                                    html.Strong(["MBFC Credibility rating:"]),
                                    " ",
                                    self.mbfc_credibility_interface(),
                                ]
                            ),
                            html.Li(
                                [
                                    html.Strong(["MBFC Reporting rating:"]),
                                    " ",
                                    self.media_ratings["reporting"],
                                ]
                            ),
                        ]
                    ),
                ]
            ),
            html.Details(
                children=[
                    html.Summary(html.A("More Info")),
                    "Media Bias/Fact Check is arguably one of the most extensive publicly available databases of media websites and their political slant/reliability, and has also been ",
                    html.A(
                        "cited",
                        href="https://doi.org/10.1093/pnasnexus/pgad286",
                        target="_top",
                    ),
                    " by academic research. More details on its methodology are available on ",
                    html.A(
                        "its official website",
                        href="https://mediabiasfactcheck.com/methodology/",
                        target="_top",
                    ),
                    ". This list is queried in real time.",
                ],
                style=app.details_style,
            ),
        ]
        return self.submit_layout


class WikipediaEnglishPerennial(MediaReliabilityCheck):
    def query_media_list(self):
        """
        Short description: Query whether a given domain is included in the English Wikipedia Perennial Sources List

        Long description: The Perennial Sources list on English Wikipedia is a table of sources whose reliability is frequently discussed by editors. More details on its methodology are available here (https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources/Perennial_sources#Legend). This data is queried in real time and includes the source name and its reliability rating.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool, pd.DataFrame): Boolean assessment for whether domain is included; Table of information from Perennial Sources list (if applicable)
        """
        # Query MediaWiki API
        S = requests.Session()
        URL = "https://en.wikipedia.org/w/api.php"
        PARAMS = {
            "action": "parse",
            "page": "Wikipedia:Reliable_sources/Perennial_sources",
            "prop": "text",
            "formatversion": "2",
            "format": "json",
        }
        R = S.get(url=URL, params=PARAMS)
        DATA = R.json()

        # Parse table
        soup = BeautifulSoup(DATA["parse"]["text"], "html.parser")
        htmltable = soup.find(
            "table", {"class": "wikitable"}
        )
        rows = htmltable.find("tbody").find_all("tr")
        df_rows = []
        for row in rows:
            cells = row.find_all("td")
            if len(cells) == 0:
                continue
            source = cells[0].get_text().strip()
            if " WP:" in source:
                source = source.split(" WP:")[0]
            status_links = cells[1].find_all("a")
            status = status_links[0].get("title")
            domain_links = cells[5].find_all("a")
            domains = []
            for link in domain_links:
                title = link.get("title")
                if title:
                    domain_search = re.search('(?<=").*(?=")', title)
                    if domain_search:
                        domains.append(domain_search.group(0))
            df_row = {}
            df_row["source"] = source
            df_row["status"] = status
            df_row["domains"] = domains
            df_rows.append(df_row)
        df = pd.DataFrame(df_rows)
        all_domains = functools.reduce(lambda x, y: x + y, df["domains"].values)
        all_domains = set(all_domains)
        self.in_media_list = set([self.domain]).issubset(all_domains)
        self.media_list_interface = "Yes" if self.in_media_list is True else "No"
        self.media_ratings = pd.DataFrame()
        if self.in_media_list:
            df["domain_check"] = df["domains"].apply(
                lambda x: set([self.domain]).issubset(x)
            )
            self.media_ratings = df[df["domain_check"]][["source", "status"]]
        return self.in_media_list, self.media_ratings

    def per_interface(self):
        """
        Color-coded output for Yes/No assessment of whether a given source is included in the English Wikipedia Perennial Sources List
            Returns:
                (dash_html_component): Color-coded output, wrapped in Dash HTML components
        """
        per_unreliable_check = (
            len(
                self.media_ratings[
                    self.media_ratings["status"].isin(
                        ["Generally unreliable", "Deprecated", "Blacklisted"]
                    )
                ]
            )
            > 0
        )
        if self.media_list_interface == "Yes" and per_unreliable_check:
            return html.Strong(
                children=html.A(self.media_list_interface), style={"color": "#ff0706"}
            )
        return html.A(self.media_list_interface)

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.submit_layout = [
            html.Li(
                [
                    html.Strong(
                        [
                            "Is domain in ",
                            html.A(
                                "English Wikipedia Perennial Sources List",
                                href="https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources/Perennial_sources",
                                target="_top",
                            ),
                            "?: ",
                        ]
                    ),
                    self.media_list_interface,
                ]
            )
        ]
        if self.in_media_list:
            style_condition = [
                {
                    "if": {"filter_query": "{status} = 'Generally reliable'"},
                    "backgroundColor": "#DDFFDD",
                },
                {
                    "if": {"filter_query": "{status} = 'No consensus'"},
                    "backgroundColor": "#FFFFDD",
                },
                {
                    "if": {"filter_query": "{status} = 'Generally unreliable'"},
                    "backgroundColor": "#FFDDDD",
                },
                {
                    "if": {"filter_query": "{status} = 'Deprecated'"},
                    "backgroundColor": "#FFBBBB",
                },
                {
                    "if": {"filter_query": "{status} = 'Blacklisted'"},
                    "backgroundColor": "#DDDDDD",
                },
            ]
            self.submit_layout = [
                html.Li(
                    [
                        html.Strong(
                            [
                                "Is domain in ",
                                html.A(
                                    "English Wikipedia Perennial Sources List",
                                    href="https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources/Perennial_sources",
                                    target="_top",
                                ),
                                "?: ",
                            ]
                        ),
                        self.per_interface(),
                    ]
                ),
                html.Br()
            ]
            tbl = app.create_table(self.media_ratings, style_condition)
            self.submit_layout.append(tbl)
        self.submit_layout.append(
            html.Details(
                children=[
                    html.Summary(html.A("More Info")),
                    "The Perennial Sources list on English Wikipedia is a table of sources whose reliability is frequently discussed by editors. More details on its methodology are available ",
                    html.A(
                        "here",
                        href="https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources/Perennial_sources#Legend",
                        target="_top",
                    ),
                    ". This data is queried in real time and includes the source name and its reliability rating. The output is presented as a table due to some sources having more than one entry.",
                ],
                style=app.details_style,
            )
        )
        self.submit_layout.append(html.Br())
        return self.submit_layout


class Decodex(MediaReliabilityCheck):
    def query_media_list(self):
        """
        Short description: Query whether a given domain is included in Le Monde's Décodex

        Long description: Décodex is a public database, developed by French newspaper Le Monde, of websites and social media pages, each labeled with a credibility indicator. While it does not appear to have been updated since 2021, it is notable for being published by an established news outlet and for a focus on France. It has been cited by research from the Reuters Institute for the Study of Journalism (https://reutersinstitute.politics.ox.ac.uk/sites/default/files/2018-02/Measuring%20the%20reach%20of%20fake%20news%20and%20online%20distribution%20in%20Europe%20CORRECT%20FLAG.pdf). More details on its methodology are available here (https://web.archive.org/web/20230321083413/https://www.lemonde.fr/les-decodeurs/article/2017/01/23/l-annuaire-des-sources-du-decodex-mode-d-emploi_5067719_4355770.html). This data is queried in real time and includes the source's reliability rating.
        Caution: While this data source flags certain social media profiles and pages as unreliable, it considers a number of social media platforms as a whole (ex. Facebook, Twitter, Reddit) as reliable in principle.
            Parameters:
                domain (str): Website domain
            Returns:
                (list): Yes/No assessment for whether domain is included; Credibility rating (if applicable), all wrapped in Dash HTML components
        """
        reliability_mapping = {
            0: "Unknown",
            1: "Satire/Parody",
            2: "Generally unreliable",
            3: "Questionable reliability",
            4: "Generally reliable in principle",
        }
        r = requests.get("http://www.lemonde.fr/webservice/decodex/updates")
        if r.status_code != 200:
            return "Le Monde's Décodex not available"
        lm = r.json()
        self.media_ratings = {}
        self.media_ratings["credibility"] = "Not available"
        self.in_media_list = set([self.domain]).issubset(set(lm["urls"].keys()))
        if self.in_media_list:
            site_index = str(lm["urls"][self.domain])
            reliability_index = lm["sites"][site_index][0]
            self.media_ratings["credibility"] = reliability_mapping[reliability_index]
        return self.in_media_list, self.media_ratings

    def decodex_credibility_interface(self):
        """
        Color-coded output for credibility rating of a given source in Le Monde's Décodex
            Parameters:
                check (str): Credibility rating of source Sources list for the given domain
            Returns:
                (dash_html_component): Color-coded output, wrapped in Dash HTML components
        """
        color_map = {
            "Satire/Parody": "#ff0706",
            "Generally unreliable": "#ff0706",
            "Questionable reliability": "#7B8705",
        }
        check = self.media_ratings["credibility"]
        if check is None or check in [
            "Not available",
            "n/a",
            "Generally reliable in principle",
        ]:
            return html.A(check)
        else:
            return html.Strong(
                children=html.A(check), style={"color": color_map[check]}
            )

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.media_list_interface = "Yes" if self.in_media_list is True else "No"
        self.submit_layout = [
            html.Li(
                [
                    html.Strong(
                        [
                            "Is domain in ",
                            html.A(
                                "Le Monde's Décodex",
                                href="https://www.lemonde.fr/verification/",
                                target="_top",
                            ),
                            "?: ",
                        ]
                    ),
                    self.media_list_interface,
                    html.Ul(
                        [
                            html.Li(
                                [
                                    html.Strong(["Décodex Credibility rating:"]),
                                    " ",
                                    self.decodex_credibility_interface(),
                                ]
                            )
                        ]
                    ),
                ]
            ),
            html.Details(
                children=[
                    html.Summary(html.A("More Info")),
                    "Décodex is a public database, developed by French newspaper Le Monde, of websites and social media pages, each labeled with a credibility indicator. While it does not appear to have been updated since 2021, it is notable for being published by an established news outlet and for a focus on France. It has been cited by ",
                    html.A(
                        "research from the Reuters Institute for the Study of Journalism",
                        href="https://reutersinstitute.politics.ox.ac.uk/sites/default/files/2018-02/Measuring%20the%20reach%20of%20fake%20news%20and%20online%20distribution%20in%20Europe%20CORRECT%20FLAG.pdf",
                        target="_top",
                    ),
                    ". More details on its methodology are available ",
                    html.A(
                        "here",
                        href="https://web.archive.org/web/20230321083413/https://www.lemonde.fr/les-decodeurs/article/2017/01/23/l-annuaire-des-sources-du-decodex-mode-d-emploi_5067719_4355770.html",
                        target="_top",
                    ),
                    ". This data is queried in real time and includes the source's reliability rating. ",
                    html.Strong(children=html.A("Caution")),
                    ": While this data source flags certain social media profiles and pages as unreliable, it considers a number of social media platforms as a whole (ex. Facebook, Twitter, Reddit) as reliable in principle.",
                ],
                style=app.details_style,
            ),
        ]
        return self.submit_layout


def n_media_reliability_red_flags(mbfc_data, per_data, decodex_data):
    """
    Counts the number of negative reliability assessments of a given website domain
        Parameters:
            mbfc_data (dict): Reliability assessment data from Media Bias/Fact-Check (MBFC)
            per_data (pd.DataFrame): Reliability assessment data from Wikipedia English Perennial Sources list
            decodex_data (dict): Reliability assessment data from Le Monde's Décodex
        Returns:
            (int): Number of media reliability sites that rate given domain as unreliable
    """
    n = 0
    # If MBFC rates a source as having low credibility or being a satire site or a fake news site, then it is considered unreliable
    if mbfc_data["credibility"] == "low-credibility" or mbfc_data["bias"] in [
        "satire",
        "fake-news",
    ]:
        n += 1

    # If Wikipedia English editors rate a source as Generally unreliable or deprecate/blocklist that source, then it is considered unreliable
    if len(per_data) > 0:
        per_unreliable_check = (
            len(
                per_data[
                    per_data["status"].isin(
                        ["Generally unreliable", "Deprecated", "Blacklisted"]
                    )
                ]
            )
            > 0
        )
        if per_unreliable_check:
            n += 1

    # If Le Monde rates a source as generally unreliable or being a satire/parody site, then it is considered unreliable
    if len(decodex_data) > 0:
        if decodex_data["credibility"] in ["Satire/Parody", "Generally unreliable"]:
            n += 1
    return n


def query_wikipedia_perennial_sources(code, page):
    """
    Generalized data extraction Wikipedia Perennial Sources Lists across multiple languages
    Tested on 15 pages as of 2024-07-27: English (en), Persian (fa), Chinese (zh), Russian (ru), Ukrainian (uk), Greek (el), French (fr), Lithuanian (lt), Portuguese (pt), Simple English (simple), Swedish (sv), Vietnamese (vi), Turkish (tr), Nepali (ne), and Bulgarian (bg)
        Parameters:
            code (str): Wikipedia code, the subdomain of wikipedia.org, represented as an ISO 639 code
            page (str): Title of Wiki page
        Returns:
            (pd.DataFrame): Data table of Perennial Sources list
    """
    # Query MediaWiki API
    URL = f"https://{code}.wikipedia.org/w/api.php"
    PARAMS = {
        "action": "parse",
        "page": page,
        "prop": "text",
        "formatversion": "2",
        "format": "json",
    }
    with requests.Session() as S:
        R = S.get(url=URL, params=PARAMS)
        DATA = R.json()

    # Parse table
    soup = BeautifulSoup(DATA["parse"]["text"], "html.parser")
    ## TODO: Fix `status` values for Chinese, Swedish, Persian and Turkish
    ## NOTE: For Turkish, `Sputnik` has more than one status value
    ## NOTE: Portuguese and Bulgarian only include sources deemed to be unreliable
    ## NOTE: French does not appear to have consensus labels for each source
    if code == "bg":
        # Bulgarian
        try:
            htmltable = soup.find_all("table", {"class": "mw-collapsible"})[1]
            if htmltable is not None:
                rows = htmltable.find("ul").find_all("li")
                domains = [row.contents[0].strip() for row in rows]
                df = pd.DataFrame({"domains": domains})
                return df
        except Exception as e:
            print(e)
            return None
    elif code == "fr":
        # French
        try:
            rows = soup.find_all("div", {"class": "NavFrame"})
            names = [row.find("div", {"class": "NavHead"}).get_text() for row in rows]
            domains = [row.find("small").get_text() for row in rows]
            assert len(names) == len(domains)
            df = pd.DataFrame({"source": names, "domains": domains})
            return df
        except Exception as e:
            print(e)
            return None
    htmltable = soup.find("table", {"class": "wikitable sortable perennial-sources"})
    if htmltable is None:
        htmltable = soup.find("table", {"class": "wikitable sortable"})
    if htmltable is None:
        htmltable = soup.find("table", {"class": "wikitable"})
    if htmltable is None:
        return None
    rows = htmltable.find("tbody").find_all("tr")
    df_rows = []
    for row in rows:
        cells = row.find_all("td")
        if len(cells) == 0:
            continue
        source = cells[0].get_text().strip()
        if " WP:" in source:
            source = source.split(" WP:")[0]
        if code == "tr":
            # Turkish
            # `status` is 3rd column, not 2nd
            if len(cells) > 2:
                status_links = cells[2].find_all("a")
            else:
                status_links = []
        else:
            status_links = cells[1].find_all("a")
        if status_links == []:
            status = None
        else:
            status = status_links[0].get("title")
        domain_links = cells[-1].find_all("a") + cells[-2].find_all("a")  # Swedish
        domains = []
        if code == "pt":
            # Portuguese
            domains = cells[-1].get_text().strip().split(", ")
        for link in domain_links:
            title = link.get("title")
            href = link.get("href")
            attrs = [title, href]
            for attr in attrs:
                if attr:
                    for regex in ['(?<=").*(?=")', "(?<=%22).*(?=%22)"]:
                        domain_search = re.search(regex, attr)
                        if domain_search:
                            domains.append(domain_search.group(0))

        df_row = {}
        df_row["source"] = source
        df_row["status"] = status
        df_row["domains"] = set(domains)
        df_rows.append(df_row)
    df = pd.DataFrame(df_rows)
    return df
