# Import Packages
## Standard Packages

## Installed Packages
import pandas as pd
import requests
from dash import dcc
from dash import html
from ipwhois import IPWhois

## Custom Libraries
import src.app as app


def query_whois(ip):
    """
    Short description: Lookup WHOIS information about website domain based on IP address

    Long description: WHOIS is a protocol for looking up registration information about a website. This data is queried in real time and includes the network routing block for the IP address (cidr), the identifier for the network registration (name), the network range for the IP address (range), the description for the registered network (description), the country code (country), state (state), city (city), mailing address (address), and postal code (postal_code), network registration date (created), and updated date for the network registration (updated).
        Parameters:
            ip (str): IP address
        Returns:
            (list): Table of WHOIS information, wrapped in Dash HTML components
    """
    table_layout = []
    table_header = html.H3(
                [
                    html.A(
                        "WHOIS Report",
                        href="https://www.whois.com/whois/",
                        target="_top",
                    ),
                    ":",
                ]
                )
    table_layout.append(table_header)
    table_details = html.Details(
                children=[
                    html.Summary(html.A("More Info")),
                    "WHOIS is a protocol for looking up registration information about a website. This data is queried in real time and includes the network routing block for the IP address (",
                    html.Em("cidr"),
                    "), the identifier for the network registration (",
                    html.Em("name"),
                    "), the network range for the IP address (",
                    html.Em("range"),
                    "), the description for the registered network (",
                    html.Em("description"),
                    "), the country code (",
                    html.Em("country"),
                    "), state (",
                    html.Em("state"),
                    "), city (",
                    html.Em("city"),
                    "), mailing address (",
                    html.Em("address"),
                    "), and postal code (",
                    html.Em("postal_code"),
                    "), network registration date (",
                    html.Em("created"),
                    "), and updated date for the network registration (",
                    html.Em("updated"),
                    ").",
                ],
                style=app.details_style,
            )
    table_layout.append(table_details)
    if ip == "IP address not found":
        return table_layout
    try:
        lookup_dict = IPWhois(ip)
        lookup_dict = lookup_dict.lookup_whois()
        df = pd.DataFrame(lookup_dict["nets"][0])
        df = df.drop(columns=["handle", "emails"])
        df = df.drop_duplicates()
        tbl = app.create_table(df)
        table_layout = [
            table_header,
            tbl,
            table_details
        ]
        return table_layout
    except:
        return table_layout


def extract_rdap_metadata(domain):
    """
    Short description: Extract domain registration dates from RDAP API (Registration Data Access Protocol)

    Long description: Registration Data Access Protocol (RDAP) is a protocol for looking up registration information about a website, and is the successor to the WHOIS protocol. Data from both WHOIS and RDAP are included as both databases are not fully consistent (https://doi.org/10.1007/978-3-031-56249-5_9). This data is queried in real time and includes the original registration date, the date the registration was last updated, the date that the domain has or will expire, and last date that the RDAP database was updated.
    Parameters:
        domain (str): Website domain
    Returns:
        (list): Table of key dates for domain registration information from RDAP, wrapped in Dash HTML components
    """
    # TODO: Ensure that number of requests to RDAP.org is less than 600 per rolling 300 second window

    object_type = "domain"
    table_layout = []
    table_details = html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Registration Data Access Protocol (RDAP) is a protocol for looking up registration information about a website, and is the successor to the WHOIS protocol. Data from both WHOIS and RDAP are included as both databases ",
                            html.A(
                                "are not fully consistent",
                                href="https://doi.org/10.1007/978-3-031-56249-5_9",
                                target="_top",
                            ),
                            ". This data is queried in real time and includes the original registration date, the date the registration was last updated, the date that the domain has or will expire, and last date that the RDAP database was updated.",
                        ],
                        style=app.details_style,
                    )
    table_layout.append(table_details)
    object_id = domain
    if object_id is None:
        return table_layout

    try:
        r = requests.get(
            "https://rdap.org/{type}/{object}".format(
                type=object_type, object=object_id
            )
        )
        if r.status_code == 200:
            if "events" in r.json().keys():
                df_rdap = pd.DataFrame(r.json()["events"])
                df_rdap["eventDate"] = pd.to_datetime(df_rdap["eventDate"])
                df_rdap = df_rdap.sort_values("eventDate")
                tbl = app.create_table(df_rdap)
                # Print table of key dates for domain registration information from RDAP
                table_layout = [
                    html.H3(
                        [
                            "Domain registration dates from ",
                            html.A(
                                "RDAP (Registration Data Access Protocol)",
                                href="https://about.rdap.org/",
                                target="_top",
                            ),
                            ":",
                        ]
                    ),
                    tbl,
                    table_details
                ]
                return table_layout
            else:
                # No domain registration info dates available
                return table_layout
        else:
            return table_layout
    except:
        return table_layout
