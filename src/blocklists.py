# Import Packages
## Standard Packages
import re
from io import StringIO

## Installed Packages
import pandas as pd
import requests
from dash import html

## Custom Libraries
import src.app as app


class BlocklistCheck:
    def __init__(self, domain):
        """
        Parameters:
            domain (str): Website domain
        """
        self.domain = domain
        self.in_blocklist = None

    def blocklist_check_interface(self):
        """
        Color-coded output for presence or absence of a domain in a blocklist
            Parameters:
                check (bool): Whether or not a domain is present in a blocklist
            Returns:
                (dash_html_component): Color-coded output, wrapped in Dash HTML components
        """
        if self.in_blocklist:
            return html.Strong(children=html.A("Is in"), style={"color": "#ff0706"})
        return html.Strong(children=html.A("Is not in"), style={"color": "#008000"})


class OpenSources(BlocklistCheck):
    def query_blocklist(self):
        """
        Short description: Query whether a given domain is included in the OpenSources database

        Long description: In 2016, media professor Melissa Zimdars produced one of the first widely known lists of websites considered to be unreliable. While it has not been updated since 2017, many of the sites on the list are still active, and the list is widely cited in academic research (ex. https://doi.org/10.1126/sciadv.aau4586, https://doi.org/10.1057/s41599-022-01174-9).
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain is included
        """
        df = pd.read_csv(
            "https://github.com/OpenSourcesGroup/opensources/raw/master/sources/sources.csv",
            names=["domain", "type_1", "type_2", "type_3", "notes_1", "notes_2"],
        )
        self.in_blocklist = set([self.domain]).issubset(df["domain"])
        return self.in_blocklist

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " ",
                    html.A(
                        "OpenSources.co",
                        href="https://github.com/OpenSourcesGroup/opensources",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "In 2016, media professor Melissa Zimdars produced one of the first widely known lists of websites considered to be unreliable. While it has not been updated since 2017, many of the sites on the list are still active, and the list is ",
                            html.A(
                                "widely",
                                href="https://doi.org/10.1126/sciadv.aau4586",
                                target="_top",
                            ),
                            " ",
                            html.A(
                                "cited",
                                href="https://doi.org/10.1057/s41599-022-01174-9",
                                target="_top",
                            ),
                            " in academic research.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class PinkSlime(BlocklistCheck):
    def query_blocklist(self):
        """
        Short description: Query whether a given domain is included in the pink slime sites database from Columbia Journalism Review

        Long description: Starting in 2019, journalists observed a number of websites posing as local news outlets in the United States that produce large amounts of low-quality content. Columbia Tow Center researcher Priyanjana Bengani collected, analyzed, and distributed a list of over 1,000 of these websites, dubbed 'pink slime' sites. This work has been cited by academic research (https://doi.org/10.1177/00027162231214696), and by many journalists (ex. https://www.texasobserver.org/pink-slime-journalism-finds-a-home-in-texas-news-deserts/, https://www.nytimes.com/2020/10/18/technology/timpone-local-news-metric-media.html). It has also been included in iffy.news and Media Bias/Fact Check. This list is queried in real time.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain is included
        """
        df = pd.read_csv(
            "https://github.com/TowCenter/partisan-local-news/raw/main/datasets/domains/all_domains.csv"
        )
        self.in_blocklist = set([self.domain]).issubset(df["domain"])
        return self.in_blocklist

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " Columbia Journalism Review's ",
                    html.A(
                        "pink slime list",
                        href="https://github.com/TowCenter/partisan-local-news",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Starting in 2019, journalists observed a number of websites posing as local news outlets in the United States that produce large amounts of low-quality content. Columbia Tow Center researcher Priyanjana Bengani collected, analyzed, and distributed a list of over 1,000 of these websites, dubbed 'pink slime' sites. This work has been cited ",
                            html.A(
                                "by academic research",
                                href="https://doi.org/10.1177/00027162231214696",
                                target="_top",
                            ),
                            " and by ",
                            html.A(
                                "many",
                                href="https://www.texasobserver.org/pink-slime-journalism-finds-a-home-in-texas-news-deserts/",
                                target="_top",
                            ),
                            " ",
                            html.A(
                                "journalists",
                                href="https://www.nytimes.com/2020/10/18/technology/timpone-local-news-metric-media.html",
                                target="_top",
                            ),
                            ". It has also been included in iffy.news and Media Bias/Fact Check. This list is queried in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class IffyNews(BlocklistCheck):
    def query_blocklist(self):
        """
        Short description: Query whether a given domain is included in the iffy.news database

        Long description: A list of sites deemed to be unreliable by independent researcher Barrett Golding, this list is also cited (https://doi.org/10.1093/pnasnexus/pgad286) in academic research. This list is queried in real time.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain is included
        """

        try:
            file_url = "https://opensheet.elk.sh/1ck1_FZC-97uDLIlvRJDTrGqBk0FuDe9yHkluROgpGS8/Iffy-news"
            iffy = requests.get(file_url)
        except Exception as e:
            print(e)
            try:
                file_url = "https://web.archive.org/web/20231204090550/https://opensheet.elk.sh/1ck1_FZC-97uDLIlvRJDTrGqBk0FuDe9yHkluROgpGS8/Iffy-news"
                iffy = requests.get(file_url)
            except Exception as e:
                print(e)
        df = pd.DataFrame.from_dict(iffy.json())
        self.in_blocklist = set([self.domain]).issubset(df["Domain"])
        return self.in_blocklist

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " ",
                    html.A("iffy.news", href="https://iffy.news/", target="_top"),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "A list of sites deemed to be unreliable by independent researcher Barrett Golding, this list is also ",
                            html.A(
                                "cited",
                                href="https://doi.org/10.1093/pnasnexus/pgad286",
                                target="_top",
                            ),
                            " in academic research. This list is queried in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class WikipediaEnglishFakeNews(BlocklistCheck):
    def get_wikipedia_english_fake_news_websites(self):
        """
        Save lists of fake news websites from Wikipedia English to CSV file
        """
        # Query MediaWiki API
        URL = "https://en.wikipedia.org/w/api.php"
        PARAMS = {
            "action": "parse",
            "prop": "text",
            "formatversion": "2",
            "format": "json",
        }

        def query_api(URL, PARAMS):
            with requests.Session() as S:
                R = S.get(url=URL, params=PARAMS)
                DATA = R.json()
            return DATA

        PARAMS["page"] = "List_of_fake_news_websites"
        DATA = query_api(URL, PARAMS)
        df_fnw = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = "List_of_corporate_disinformation_website_campaigns"
        DATA = query_api(URL, PARAMS)
        df_cdwc = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = "List_of_political_disinformation_website_campaigns"
        DATA = query_api(URL, PARAMS)
        df_pdwc = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = "Fake_news_in_the_Philippines"
        DATA = query_api(URL, PARAMS)
        df_p = pd.read_html(StringIO(DATA["parse"]["text"]))[2]
        df_p = df_p.rename(columns={"URL": "Domain"})

        PARAMS["page"] = "List_of_political_disinformation_website_campaigns_in_Russia"
        DATA = query_api(URL, PARAMS)
        df_r = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = (
            "List_of_political_disinformation_website_campaigns_in_the_United_States"
        )
        DATA = query_api(URL, PARAMS)
        df_usa = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = "List_of_satirical_fake_news_websites"
        DATA = query_api(URL, PARAMS)
        df_sfnw = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = "List_of_fake_news_troll_farms"
        DATA = query_api(URL, PARAMS)
        df_fntf = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:-9])

        PARAMS["page"] = "List_of_miscellaneous_fake_news_websites"
        DATA = query_api(URL, PARAMS)
        df_mfnw = pd.read_html(StringIO(DATA["parse"]["text"]))[0]

        df = pd.concat(
            [df_fnw, df_cdwc, df_pdwc, df_p, df_r, df_usa, df_sfnw, df_fntf, df_mfnw]
        )

        df["domain_enr"] = (
            df["Domain"].str.replace("\[.\]", ".", regex=True).str.strip()
        )
        df["domain_enr"] = df["domain_enr"].str.replace("^www[.]", "", regex=True)
        df["domain_enr"] = df["domain_enr"].str.lower()
        df = df.drop_duplicates(["domain_enr"])
        df = df[~pd.isnull(df["domain_enr"])]
        df.to_csv("src/data/Wikipedia_fake_news_websites.csv", index=False)

    def query_blocklist(self):
        """
        Short description: Query whether a given domain is included in the lists of fake news websites from English Wikipedia

        Long description: Based on contributions from Wikipedia English editors, checks if the website is included in a number of related lists, including "List of fake news websites" (https://en.wikipedia.org/wiki/List_of_fake_news_websites), "List of corporate disinformation website campaigns" (https://en.wikipedia.org/wiki/List_of_corporate_disinformation_website_campaigns), "List of political disinformation website campaigns (https://en.wikipedia.org/wiki/List_of_political_disinformation_website_campaigns), "List of satirical fake news websites" (https://en.wikipedia.org/wiki/List_of_satirical_fake_news_websites), "List of fake news troll farms" (https://en.wikipedia.org/wiki/List_of_fake_news_troll_farms"), and "List of miscellaneous fake news websites" (https://en.wikipedia.org/wiki/List_of_miscellaneous_fake_news_websites). The lists are queried every 2 weeks.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain is included
        """
        df = pd.read_csv("src/data/Wikipedia_fake_news_websites.csv")
        self.in_blocklist = set([self.domain]).issubset(df["domain_enr"])
        return self.in_blocklist

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " English Wikipedia's ",
                    html.A(
                        "lists of fake news sites",
                        href="https://en.wikipedia.org/wiki/List_of_fake_news_websites",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Based on contributions from Wikipedia English editors, checks if the website is included in a number of related lists, including ",
                            html.A(
                                "List of fake news websites",
                                href="https://en.wikipedia.org/wiki/List_of_fake_news_websites",
                                target="_top",
                            ),
                            ", ",
                            html.A(
                                "List of corporate disinformation website campaigns",
                                href="https://en.wikipedia.org/wiki/List_of_corporate_disinformation_website_campaigns",
                                target="_top",
                            ),
                            ", ",
                            html.A(
                                "List of political disinformation website campaigns",
                                href="https://en.wikipedia.org/wiki/List_of_political_disinformation_website_campaigns",
                                target="_top",
                            ),
                            ", ",
                            html.A(
                                "List of satirical fake news websites",
                                href="https://en.wikipedia.org/wiki/List_of_satirical_fake_news_websites",
                                target="_top",
                            ),
                            ", ",
                            html.A(
                                "List of fake news troll farms",
                                href="https://en.wikipedia.org/wiki/List_of_fake_news_troll_farms",
                                target="_top",
                            ),
                            ", and ",
                            html.A(
                                "List of miscellaneous fake news websites",
                                href="https://en.wikipedia.org/wiki/List_of_miscellaneous_fake_news_websites",
                                target="_top",
                            ),
                            ". The lists are queried every 2 weeks.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class WikipediaEnglishSatiricalNews(BlocklistCheck):
    def get_wikipedia_english_satirical_news_websites(self):
        """
        Save lists of satirical news websites from Wikipedia English to CSV file
        """
        # Query MediaWiki API
        URL = "https://en.wikipedia.org/w/api.php"
        PARAMS = {
            "action": "parse",
            "page": "List_of_satirical_news_websites",
            "prop": "text",
            "formatversion": "2",
            "format": "json",
        }
        with requests.Session() as S:
            R = S.get(url=URL, params=PARAMS)
            DATA = R.json()
        df = pd.concat(pd.read_html(StringIO(DATA["parse"]["text"]))[0:2])
        df["domain_enr"] = (
            df["Domain"].str.replace("\[.\]", ".", regex=True).str.strip()
        )
        df["domain_enr"] = df["domain_enr"].str.lower()
        df = df.drop_duplicates(["domain_enr"])
        df = df[~pd.isnull(df["domain_enr"])]
        df.to_csv("src/data/Wikipedia_satirical_news_websites.csv", index=False)

    def query_blocklist(self):
        """
        Short description: Query whether a given domain is included in the list of satirical news websites from English Wikipedia

        Long description: Based on contributions from Wikipedia English editors, checks if the website is included in the list of satirical news websites (https://en.wikipedia.org/wiki/List_of_satirical_news_websites). The list is queried every 2 weeks.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain is included
        """
        df = pd.read_csv("src/data/Wikipedia_satirical_news_websites.csv")
        self.in_blocklist = set([self.domain]).issubset(df["domain_enr"])
        return self.in_blocklist

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " English Wikipedia's ",
                    html.A(
                        "list of satirical news sites",
                        href="https://en.wikipedia.org/wiki/List_of_satirical_news_websites",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Based on contributions from Wikipedia English editors, checks if the website is included in the list of satirical news websites. The list is queried every 2 weeks.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class WikipediaCzechFakeNews(BlocklistCheck):
    def query_blocklist(self):
        """
        Short description: Query whether a given domain is included in the lists of fake news websites from Czech Wikipedia

        Long description: Based on contributions from Wikipedia Czech editors, checks if the website is included in the list of disinformation websites (https://cs.wikipedia.org/wiki/Seznam_dezinforma%C4%8Dn%C3%ADch_web%C5%AF_v_%C4%8De%C5%A1tin%C4%9B). The list is queried in real time.
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain is included
        """
        # Query MediaWiki API
        URL = "https://cs.wikipedia.org/w/api.php"
        PARAMS = {
            "action": "parse",
            "page": "Seznam_dezinformačních_webů_v_češtině",
            "prop": "text",
            "formatversion": "2",
            "format": "json",
        }
        with requests.Session() as S:
            R = S.get(url=URL, params=PARAMS)
            DATA = R.json()
        df = pd.read_html(StringIO(DATA["parse"]["text"]))[0]
        df["domain_enr"] = (
            df["URL"]
            .str.replace("^https://", "", regex=True)
            .replace("^www[.]", "", regex=True)
            .replace("/$", "", regex=True)
        )
        self.in_blocklist = set([self.domain]).issubset(df["domain_enr"])
        return self.in_blocklist

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " Czech Wikipedia's ",
                    html.A(
                        "list of Czech disinformation sites",
                        href="https://cs.wikipedia.org/wiki/Seznam_dezinforma%C4%8Dn%C3%ADch_web%C5%AF_v_%C4%8De%C5%A1tin%C4%9B",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Based on contributions from Wikipedia Czech editors, checks if the website is included in the list of disinformation websites. The list is queried in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


# Wikimedia Global Spam Blacklist
wikimedia_bl_url = (
    "https://meta.wikimedia.org/w/index.php?title=Spam_blacklist&action=raw&sb_ver=1"
)

# English Wikipedia Local Spam Blacklist
en_wikipedia_bl_url = "https://en.wikipedia.org/w/index.php?title=MediaWiki:Spam-blacklist&action=raw&sb_ver=1"


class WikimediaSpamBlocklist(BlocklistCheck):
    def query_blocklist(self, bl_url):
        """
        Short description: Query whether a given domain is included in the given Wikimedia spam blocklist

        Long description: Checks whether the website is in a spam blocklist (https://www.mediawiki.org/wiki/Extension:SpamBlacklist) that is used for a given Wikimedia project. The purpose is to prevent edits that contain that website domain from being published. This list is queried in real time.
            Parameters:
                domain (str): Website domain
                bl_url (str): URL of Wikimedia spam blocklist
            Returns:
                (bool): Binary assessment for whether domain is included
        """
        r = requests.get(bl_url)
        if r.status_code == 404:
            return "Not available"
        bl_regex = r.text.split("\n")
        bl_regex = list(filter(lambda x: not x.strip().startswith("#"), bl_regex))
        bl_regex = list(
            filter(lambda x: x.split("#")[0].strip() if "#" in x else x, bl_regex)
        )
        bl_regex = [x.split("#")[0].strip() if "#" in x else x for x in bl_regex]
        bl_regex = list(filter(lambda x: x.strip() != "", bl_regex))
        # Replace infinite width lookbehind with fixed-width lookbehind
        bl_regex = [x.replace("(?<=//|\\.)", "(?<=[//\\.])") for x in bl_regex]
        bl_regex = [x.replace("(?<=\\.|://)", "(?<=[//\\.])") for x in bl_regex]
        bl_regex = [x.replace("\\x{23}", "#") for x in bl_regex]
        for reg in bl_regex:
            try:
                match_found = re.search(reg, self.domain)
            except re.error:
                match_found = re.search(re.escape(reg), self.domain)
            if match_found:
                self.in_blocklist = True
                return self.in_blocklist
        self.in_blocklist = False
        return self.in_blocklist

    def query_all_wm_blocklists(self):
        """
        Wrapper to query Wikipedia Spam Blacklists across all languages
            Parameters:
                domain (str): Website domain
        """
        # TODO: Print out full name of Wikipedia site
        # TODO: Speed optimization (currently takes 12 minutes in serial)
        S = requests.Session()
        URL = "https://commons.wikimedia.org/w/api.php"
        PARAMS = {
            "action": "parse",
            "page": "Data:Wikipedia_statistics/data.tab",
            "prop": "text",
            "formatversion": "2",
            "format": "json",
        }
        R = S.get(url=URL, params=PARAMS)
        DATA = R.json()
        df_wikis = pd.read_html(StringIO(DATA["parse"]["text"]))[0]
        wikipedia_list = df_wikis[
            df_wikis["site"]["string"]["site"].str.contains("wikipedia")
        ]["site"]["string"]["site"].values.tolist()
        for x in ["total.wikipedia", "totalactive.wikipedia", "totalclosed.wikipedia"]:
            wikipedia_list.remove(x)
        for wiki in wikipedia_list:
            url = f"https://{wiki}.org/w/index.php?title=MediaWiki:Spam-blacklist&action=raw&sb_ver=1"
            query_wm_bl(domain=self.domain, bl_url=url)


class WikimediaGlobalSpamBlocklist(WikimediaSpamBlocklist):
    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " Wikimedia's ",
                    html.A(
                        "Global Spam Blacklist",
                        href="https://meta.wikimedia.org/w/index.php?title=Spam_blacklist",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Checks whether the website is in a ",
                            html.A(
                                "spam blocklist",
                                href="https://www.mediawiki.org/wiki/Extension:SpamBlacklist",
                                target="_top",
                            ),
                            " that is used for all Wikimedia Foundation projects. The purpose is to prevent edits that contain that website domain from being published. This list is queried in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class WikipediaEnglishSpamBlocklist(WikimediaSpamBlocklist):
    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.blocklist_interface = self.blocklist_check_interface()
        self.submit_layout = [
            html.Li(
                children=[
                    self.blocklist_interface,
                    " English Wikipedia's ",
                    html.A(
                        "Local Spam Blacklist",
                        href="https://en.wikipedia.org/w/index.php?title=MediaWiki:Spam-blacklist",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Similar to the Wikimedia Global Spam Blacklist, but specific to Wikipedia English. This list is queried in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


def n_blocklist_hits(*args):
    """
    Counts the number of blocklists that contain a given domain
    """
    n = 0
    for arg in args:
        n += arg
    return n


assert n_blocklist_hits() == 0
assert n_blocklist_hits(True) == 1
assert n_blocklist_hits(True, False) == 1
assert n_blocklist_hits(True, True) == 2
