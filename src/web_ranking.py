# Import Packages
## Standard Packages

## Installed Packages
from dash import html
import pandas as pd
import requests

## Custom Libraries
import src.app as app


def get_tranco_rank(domain):
    """
    Short description: Queries the most recent Tranco rank of a given website domain

    Long description: Tranco is an open source website popularity ranking system that combines five separate lists of ranked sites: Cisco Umbrella, Majestic, Farsight, Chrome User Experience Report, and Cloudflare Radar. The intention is to produce a list that is less prone to manipulation. This data is queried in real time and includes the site rank as of the most recent date.
        Parameters:
            domain (str): Website domain
        Returns:
            (str): Table of Tranco rank information, wrapped in Dash HTML components
    """
    table_layout = []
    table_details = html.Details(
                    children=[
                        html.Summary(html.A("More Info")),
                        "Tranco is an open source website popularity ranking system that combines five separate lists of ranked sites: Cisco Umbrella, Majestic, Farsight, Chrome User Experience Report, and Cloudflare Radar. The intention is to produce a list that is less prone to manipulation. This data is queried in real time and includes the site rank as of the most recent date.",
                    ],
                    style=app.details_style,
                )
    table_layout.append(table_details)
    try:
        r = requests.get(f"https://tranco-list.eu/api/ranks/domain/{domain}")
        if r.status_code == 200:
            json_output = r.json()
            df = pd.DataFrame.from_dict(json_output["ranks"][0].values()).T
            df.columns = ["Date", "Rank"]
            tbl = app.create_table(df)
            table_layout = [
                html.H3(
                    [
                        html.A(
                            "Tranco Rank", href="https://tranco-list.eu/", target="_top"
                        ),
                        ":",
                    ]
                ),
                tbl,
                table_details
            ]
        return table_layout
    except:
        return table_layout
