# Import Packages
## Standard Packages
from collections import Counter
from urllib.parse import urlparse

## Installed Packages
from dash import html
import pandas as pd
import requests
from bs4 import BeautifulSoup

## Custom Libraries
import src.app as app


def get_outbound_domains(url, domain):
    """
    Shows all of the unique external webpage links (all domains except for original domain) from the given URL, sorted in descending order of frequency. This query occurs in real time.
        Parameters:
            url (str): Website URL
            domain (str): Website domain
        Returns:
            (str): Table of domains and the corresponding numbers of links on the website page, wrapped in Dash HTML components
    """
    table_layout = []
    table_details = html.Details(
            children=[
                html.Summary(html.A("More Info")),
                "Shows all of the unique external webpage links from the given URL, sorted in descending order of frequency. This query occurs in real time.",
            ],
            style=app.details_style,
        )
    table_layout.append(table_details)
    try:
        assert domain in url  # TODO: How to handle for URL shorteners, redirects, etc.?
    except Exception as e:
        print(e)
        return table_layout
    try:
        r = requests.get(url)
    except requests.exceptions.ConnectionError:
        return table_layout
    soup = BeautifulSoup(r.content, "html.parser")
    links = soup.find_all("a")
    links = [ln.get("href") for ln in links if ln.get("href")]
    links = [ln.split("#")[0] for ln in links]
    links = [ln for ln in links if domain not in ln]
    links = [urlparse(ln).netloc for ln in links]
    links = [ln for ln in links if ln.strip() != ""]
    links = [ln.replace(".", "[.]") for ln in links]
    links = [ln.lower() for ln in links]
    links = Counter(links)
    df = pd.DataFrame.from_dict(links.items())
    if len(df) == 0:
        return table_layout
    df.columns = ["Domain", "Number of Links"]
    df = df.sort_values(by=["Number of Links"], ascending=False)
    tbl = app.create_table(df)
    table_layout = [
        html.H3("Outbound Domains:"),
        tbl,
        table_details
    ]
    return table_layout
