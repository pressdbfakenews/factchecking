# Import Packages
## Standard Packages

## Installed Packages
from dash import dcc
from dash import html
import requests

## Custom Libraries
import src.app as app


def allowlist_check_interface(check):
    """
    Color-coded output for presence or absence of a domain in an allowlist
        Parameters:
            check (bool): Whether or not a domain is present in an allowlist
        Returns:
            (dash_html_component): Color-coded output, wrapped in Dash HTML components
    """
    if check:
        return html.Strong(children=html.A("Is in"), style={"color": "#008000"})
    return html.Strong(children=html.A("Is not in"), style={"color": "#ff0706"})


def file_check_interface(check):
    """
    Color-coded output for presence or absence of a given file
        Parameters:
            check (bool): Whether or not a website contains a file
        Returns:
            (dash_html_component): Color-coded output, wrapped in Dash HTML components
    """
    if check:
        return html.Strong(children=html.A("Has"), style={"color": "#008000"})
    return html.Strong(children=html.A("Does not have"), style={"color": "#ff0706"})


class TrustTxt:
    """
    JournalList is a non-profit that maintains trust.txt, a specification for news organizations to list the collectives and associations to which they belong. This query checks if the website is a publication member (i.e., a news publisher) in JournalList's own trust.txt file. This list is queried in real time.
    """

    def __init__(self, domain):
        """
        Parameters:
            domain (str): Website domain
        """
        self.domain = domain
        self.trust_url = None
        self.trust_vals = None
        self.trust_layout = html.Div([])

    def query_trust(self):
        """
        Check whether given website domain is listed as a publication member in JournalList's trust.txt file.
        """
        try:
            r = requests.get("https://journallist.net/trust.txt")
        except requests.exceptions.ConnectionError:
            r = requests.get(
                "https://web.archive.org/web/20240302035343/https://journallist.net/trust.txt"
            )
        trust = r.text.split("\n#")
        association_members = []
        for element in trust:
            element_list = element.split("\n")
            if element_list[0] == " Publication members":
                for idx, member in enumerate(element_list):
                    if idx > 0:
                        site = member.split("member=")[-1].lower()
                        association_members.append(site)
        domains = list(
            map(lambda x: app.get_domain(x, url_shortener=False), association_members)
        )
        self.has_trust_txt = set([self.domain]).issubset(domains)
        if self.has_trust_txt:
            self.trust_url = f"https://{self.domain}/trust.txt"
        return self.has_trust_txt

    def get_trust_vals(self):
        """
        Extract attributes from a given website's trust.txt file.
        """
        if self.trust_url is None:
            return None
        r = requests.get(self.trust_url)
        if r.status_code == 200:
            trust_dict = {}
            trust = r.text.split("\n#")
            for line in trust:
                element_list = line.split("\n")
                for element in element_list:
                    if "=" in element:
                        (key, val) = element.split("=")
                        if key not in trust_dict.keys():
                            trust_dict[key] = []
                        trust_dict[key].append(val)
            self.trust_vals = trust_dict
            return self.trust_vals
        return None

    def layout_trust_vals(self):
        """
        Output a given website's trust.txt file attributes.
        """
        trust_output = []
        if self.trust_vals is None:
            return self.trust_layout
        if len(self.trust_vals) > 0:
            trust_output.append(dcc.Markdown("**Trust.txt Attributes:**"))
        for key in self.trust_vals.keys():
            trust_output.append(dcc.Markdown(f"    * **{key}**"))
            for val in self.trust_vals[key]:
                trust_output.append(dcc.Markdown(f"        * {val}"))
        self.trust_layout = html.Div(trust_output)
        return self.trust_layout

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.has_trust_txt_interface = allowlist_check_interface(self.has_trust_txt)
        self.submit_layout = [
            html.Li(
                children=[
                    self.has_trust_txt_interface,
                    " JournalList's ",
                    html.A("trust.txt", href="https://journallist.net/", target="_top"),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "JournalList is a non-profit that maintains trust.txt, a specification for news organizations to list the collectives and associations to which they belong. This query checks if the website is a publication member (i.e., a news publisher) in JournalList's own trust.txt file. This list is queried in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout


class AdsTxt:
    """
    Checks if the website has an ads.txt file, a specification from the Interactive Advertising Bureau that shows which companies are allowed to advertise on the site. Note that the presence of an ads.txt file is not necessarily an indicator of reliability. This query occurs in real time.
    """

    def __init__(self, domain):
        """
        Parameters:
            domain (str): Website domain
        """
        self.domain = domain
        self.ads_url = None
        self.has_ads_txt = None

    def ads_txt_check(self):
        """
        Query whether a given domain has an ads.txt file
            Parameters:
                domain (str): Website domain
            Returns:
                (bool): Binary assessment for whether domain has an ads.txt file
        """
        try:
            test_url = f"https://{self.domain}/ads.txt"
            r = requests.get(test_url)
            if r.status_code == 200:
                self.ads_url = test_url
                self.has_ads_txt = True
            else:
                self.has_ads_txt = False
        except:
            self.has_ads_txt = False
        return self.has_ads_txt

    def get_app_layout(self):
        """
        Output for Dash app
        """
        self.has_ads_txt_interface = file_check_interface(self.has_ads_txt)
        self.submit_layout = [
            html.Li(
                children=[
                    self.has_ads_txt_interface,
                    " an ",
                    html.A(
                        "ads.txt file",
                        href="https://iabtechlab.com/ads-txt/",
                        target="_top",
                    ),
                    ".",
                    html.Details(
                        children=[
                            html.Summary(html.A("More Info")),
                            "Checks if the website has an ads.txt file, a specification from the Interactive Advertising Bureau that shows which companies are allowed to advertise on the site. Note that the presence of an ads.txt file is not necessarily an indicator of reliability. This query occurs in real time.",
                        ],
                        style=app.details_style,
                    ),
                ]
            )
        ]
        return self.submit_layout
