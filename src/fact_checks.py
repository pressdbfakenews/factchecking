# Import Packages
## Standard Packages

## Installed Packages
from dash import html

## Custom Libraries
import src.app as app


def query_claimskg(domain):
    """
    Short description: Query ClaimKG for recent fact-checks involving a given domain.

    ClaimsKG is an open source database of claims and their corresponding fact-checks from 12 major fact-checking organizations from 1996 to January 2023. This query checks the top 10 most recent fact-checked claims that use the input website as a citation. It returns the date that the fact-check of the claim was published, the original claim, the citation URL, the URL of the fact-check, and a normalized verdict by the fact-checker. More details about the data and methodology are given in the corresponding research paper (https://users.ics.forth.gr/~fafalios/files/pubs/ISWC2019_ClaimsKG.pdf). Note that presence as a citation is not necessarily an indicator of reliability, as a citation can be used as evidence for/against a claim, or documentation for where a claim originated or spread. This data is queried in real time.
        Parameters:
            domain (str): Website domain
        Returns:
            (str): Table of ClaimsKG fact-checks, wrapped in Dash HTML components
    """
    endpoint_url = "https://data.gesis.org/claimskg/sparql"
    query = """
    PREFIX itsrdf:<https://www.w3.org/2005/11/its/rdf#>
    PREFIX schema:<http://schema.org/>
    PREFIX dbr:<http://dbpedia.org/resource/>
    SELECT ?date ?claim_text ?citation ?reviewurl  ?rating_normalized
     WHERE {{ 
            ?claim a schema:CreativeWork ; schema:datePublished ?date .
            ?claim schema:author ?author ; schema:text ?text ; schema:citation ?citation . 
            BIND(STR(?text) AS ?claim_text) .
            FILTER( REGEX(?citation, "[/.]{domain}" )).
            ?claimReview schema:itemReviewed ?claim ; schema:url ?reviewurl .
            ?claimReview schema:reviewRating ?rating FILTER(REGEX(?rating, "normalized")).
            ?rating schema:alternateName ?ratingNormalized .
            BIND(STR(?ratingNormalized) AS ?rating_normalized)
          }}
    ORDER BY DESC(?date)
    LIMIT 10
    """.format(
        domain=domain
    )
    table_layout = []
    table_details = html.Details(
            children=[
                html.Summary(html.A("More Info")),
                "ClaimsKG is an open source database of claims and their corresponding fact-checks from 12 major fact-checking organizations from 1996 to January 2023. This query checks the top 10 most recent fact-checked claims that use the input website as a citation. It returns the date that the fact-check of the claim was published, the original claim, the citation URL, the URL of the fact-check, and a normalized verdict by the fact-checker. More details about the data and methodology are given in the corresponding ",
                html.A(
                    "research paper",
                    href="https://users.ics.forth.gr/~fafalios/files/pubs/ISWC2019_ClaimsKG.pdf",
                    target="_top",
                ),
                ". Note that presence as a citation is not necessarily an indicator of reliability, as a citation can be used as evidence for/against a claim, or documentation for where a claim originated or spread. This data is queried in real time.",
            ],
            style=app.details_style,
        )
    table_layout.append(table_details)
    try:
        df = app.get_sparql_query_results(endpoint_url, query)
    except Exception as e:
        return table_layout
    if len(df) == 0:
        return table_layout
    tbl = app.create_table(df)
    table_layout = [
        html.H3(
            [
                "Top 10 Most Recent Fact-Checked Claims Associated with Domain from ",
                html.A(
                    "ClaimsKG",
                    href="https://data.gesis.org/claimskg/site/",
                    target="_top",
                ),
                ":",
            ]
        ),
        tbl,
        table_details
    ]
    return table_layout
