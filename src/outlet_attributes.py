# Import Packages
## Standard Packages

## Installed Packages
from dash import html

## Custom Libraries
import src.app as app


def query_wikidata(domain):
    """
    Short description: Query WikiData for attributes about a given domain.

    Long description: Queries metadata about a given outlet from WikiData based on its website domain, including the date it was formed (inception), the name of the site founder(s) (founded), the country of origin (country), the organization or organizations that the outlet is a member of (member), its political slant (politics), the organization or organizations that own the site (owned), and the organization or organizations that are a subsidiary of the parent organization that owns the site (subsidiary). This query occurs in real time.
        Parameters:
            domain (str): Website domain
        Returns:
            (str): Table of WikiData attributes, wrapped in Dash HTML components
    """
    # TODO: Show WikiData strings and not just labels
    endpoint_url = "https://query.wikidata.org/sparql"
    query = """SELECT DISTINCT ?item
                               ?itemLabel
                               ?websiteLabel
                               ?inceptionLabel
                               ?foundedLabel
                               ?countryLabel
                               ?memberLabel
                               ?politicsLabel
                               ?ownedLabel
                               ?subsidiaryLabel
                WHERE {{
  {{
    SELECT DISTINCT ?item
       ?itemLabel
       ?websiteLabel
       ?inceptionLabel
       ?foundedLabel
       ?countryLabel
       ?memberLabel
       ?politicsLabel
       ?ownedLabel
       ?subsidiaryLabel 
    WHERE {{
      {{
        ?item p:P31 ?statement0.
        ?statement0 (ps:P31/(wdt:P279*)) wd:Q17232649.
        ?item wdt:P856 ?website.
      }}
      UNION
      {{
        ?item p:P31 ?statement1.
        ?statement1 (ps:P31/(wdt:P279*)) wd:Q27881073.
        ?item wdt:P856 ?website.
      }}
      UNION
      {{
        ?item p:P31 ?statement2.
        ?statement2 (ps:P31/(wdt:P279*)) wd:Q192283.
        ?item wdt:P856 ?website.
      }}
      UNION
      {{
        ?item p:P31 ?statement3.
        ?statement3 (ps:P31/(wdt:P279*)) wd:Q11032.
        ?item wdt:P856 ?website.
      }}
      UNION
      {{
        ?item p:P31 ?statement4.
        ?statement4 (ps:P31/(wdt:P279*)) wd:Q14350.
        ?item wdt:P856 ?website.
      }}
      FILTER( REGEX(STR(?website), "[/.]{domain}" )).
        OPTIONAL {{ ?item wdt:P571 ?inception. }}
  OPTIONAL {{ ?item wdt:P112 ?founded. }}
  OPTIONAL {{ ?item wdt:P17 ?country. }}
  OPTIONAL {{ ?item wdt:P463 ?member. }}
  OPTIONAL {{ ?item wdt:P1387 ?politics. }}
  OPTIONAL {{ ?item wdt:P127/wdt:P749* ?owned. }}
  OPTIONAL {{ ?item wdt:P355 ?subsidiary. }}
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
    }}
    LIMIT 10
  }}
    }}""".format(
        domain=domain
    )
    table_layout = []
    table_details = html.Details(
            children=[
                html.Summary(html.A("More Info")),
                "Queries metadata about a given outlet from WikiData based on its website domain, including the date it was formed (",
                html.Em("inception"),
                "), the name of the site founder(s) (",
                html.Em("founded"),
                "), the country of origin (",
                html.Em("country"),
                "), the organization or organizations that the outlet is a member of (",
                html.Em("member"),
                "), its political slant (",
                html.Em("politics"),
                "), the organization or organizations that own the site (",
                html.Em("owned"),
                "), and the organization or organizations that are a subsidiary of the parent organization that owns the site (",
                html.Em("subsidiary"),
                "). This query occurs in real time.",
            ],
            style=app.details_style,
        )
    table_layout.append(table_details)
    try:
        df = app.get_sparql_query_results(endpoint_url, query, wikidata_flag=True)
    except Exception as e:
        print(e)
        return table_layout
    if len(df) == 0:
        return table_layout
    tbl = app.create_table(df)
    table_layout = [
        html.H3(
            [
                html.A(
                    "Attributes from WikiData",
                    href="https://www.wikidata.org/wiki/Wikidata:Main_Page",
                    target="_top",
                ),
                ":",
            ]
        ),
        tbl,
        table_details
    ]
    return table_layout
