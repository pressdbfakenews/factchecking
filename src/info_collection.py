# Import Packages
## Standard Packages
import re
import socket

## Installed Packages
import requests
import tldextract

## Custom Libraries
import src.app as app

shortening_services = (
    r"bit\.ly|goo\.gl|shorte\.st|go2l\.ink|x\.co|ow\.ly|t\.co|tinyurl|tr\.im|is\.gd|cli\.gs|"
    r"yfrog\.com|migre\.me|ff\.im|tiny\.cc|url4\.eu|twit\.ac|su\.pr|twurl\.nl|snipurl\.com|"
    r"short\.to|BudURL\.com|ping\.fm|post\.ly|Just\.as|bkite\.com|snipr\.com|fic\.kr|loopt\.us|"
    r"doiop\.com|short\.ie|kl\.am|wp\.me|rubyurl\.com|om\.ly|to\.ly|bit\.do|t\.co|lnkd\.in|db\.tt|"
    r"qr\.ae|adf\.ly|goo\.gl|bitly\.com|cur\.lv|tinyurl\.com|ow\.ly|bit\.ly|ity\.im|q\.gs|is\.gd|"
    r"po\.st|bc\.vc|twitthis\.com|u\.to|j\.mp|buzurl\.com|cutt\.us|u\.bb|yourls\.org|x\.co|"
    r"prettylinkpro\.com|scrnch\.me|filoops\.info|vzturl\.com|qr\.net|1url\.com|tweez\.me|v\.gd|"
    r"tr\.im|link\.zip\.net"
)
# From https://github.com/shreyagopal/Phishing-Website-Detection-by-Machine-Learning-Techniques/blob/master/URLFeatureExtraction.py


def uses_url_shortener(url):
    """
    Checks whether or not a URL is a url shortener (ex. bit.ly)
         Parameters:
             url (str): Website URL
         Returns:
             (int): Boolean label
    """
    match = re.search(shortening_services, url)
    if match:
        return 1
    return 0


def get_domain(url):
    """
    Extract domain from a URL
        Parameters:
            url (str): Website URL
        Returns:
            (str): Website domain
    """
    if uses_url_shortener(url):
        try:
            url = requests.head(url).headers["location"]
        except requests.exceptions.ConnectionError:
            pass
        except KeyError:
            pass

    # Remove Wayback Machine domain
    ia_domain = "web.archive.org/"
    if ia_domain in url:
        url = re.sub("https\:\/\/web\.archive\.org\/web\/[0-9]{1,}/", "", url)

    try:
        ext = tldextract.extract(url)
        domain = ext.registered_domain
        return domain
    except:
        return None


def sanitize_url(url):
    """
    Sanitize a URL to prevent it from being clicked on, due to spam, malware, etc.
        Parameters:
            url (str): Website URL
        Returns:
            (str): Sanitized URL
    """
    url = url.replace(".", "[.]")
    url = re.sub("http://", "hxxp://", url)
    url = re.sub("https://", "hxxps://", url)
    return url


def query_ip_address(domain):
    """
    Lookup IP address from given website domain
        Parameters:
            domain (str): Website domain
        Returns:
            (str): IP address
    """
    try:
        ip = socket.gethostbyname(domain)
        return ip
    except:
        return "IP address not found"


def input_validation(value):
    """
    Check that URL has HTTP or HTTPS as its scheme
        Parameters:
            value (str): Input URL
        Returns:
            (str, str): URL and website domain
    """
    value = value.lower()
    http_check = re.match("^https?://", value)
    if http_check is None:
        value = "https://" + value
    domain = get_domain(url=value)
    return value, domain
