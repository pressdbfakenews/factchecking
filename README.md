Veri-FYI
=============

# What is Veri.FYI?

Veri.FYI (pronounced *verify*) is a prototype misinformation risk web platform based on publicly available data.

It takes a URL or domain (ex. 'https://example.com' or 'example.com') as input and will output indicators that can be used to assess the source's overall reliability:

- Is the website on any spam blocklists ([the WikiMedia Global Spam Blacklist](https://meta.wikimedia.org/w/index.php?title=Spam_blacklist) and the [English Wikipedia Local Spam Blacklist](https://en.wikipedia.org/w/index.php?title=MediaWiki:Spam-blacklist&action=raw&sb_ver=1))?
- Is the website on any major lists of unreliable/fake news websites ([OpenSources.co](https://github.com/OpenSourcesGroup/opensources), [Columbia Journalism Review](https://github.com/TowCenter/partisan-local-news), [iffy.news](https://iffy.news/), [English Wikipedia](https://en.wikipedia.org/wiki/List_of_fake_news_websites), [Czech Wikipedia](https://cs.wikipedia.org/wiki/Seznam_dezinforma%C4%8Dn%C3%ADch_web%C5%AF_v_%C4%8De%C5%A1tin%C4%9B))?
- Is the website cited by fact-checkers when reviewing suspect claims being made online ([ClaimsKG](https://data.gesis.org/claimskg/site/))?
- What information, if any, is available about the website's reliability ([Media Bias/Fact-Check](https://mediabiasfactcheck.com/), [trust.txt](https://journallist.net/), [English Wikipedia's Perennial Sources](https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources/Perennial_sources), [Le Monde's Décodex](https://www.lemonde.fr/verification/))?
- What information, if any, is available about the media source (ex. Ownership or political slant, all from [WikiData](https://wikidata.org/))?
- What information, if any, is available about the website's domain registration? ([WHOIS](https://www.whois.com/whois/), [RDAP](https://about.rdap.org/))
- What other websites is this website linking to, and how often?
- What is the website's ranking, based on [Tranco Rank](https://tranco-list.eu/)?
- Does the website have an [ads.txt](https://iabtechlab.com/ads-txt/) file?

# Why Veri.FYI matters

There are a lot of existing tools for online misinformation detection. However, many of them are based on lists or methodologies that are opaque, out-of-date, or not necessarily aligned with the way that journalists tend to work. Furthermore, journalists, fact-checkers and other researchers already use dozens of open source intelligence tools for online misinformation detection (among other tasks) that may work well individually, but are collectively inefficient: they may not be mutually compatible, they can be costly in aggregate, each one may require signing up for a separate account/API key, etc.

What makes Veri.FYI different is that the datasets, tools, and tech stack that it uses are itself free and open source (no paywall, etc.), and that it is starting out as an aggregation of existing, generally reliable, and open source assessments of source credibility. The goal is for Veri.FYI to be a streamlined OSINT application for assessing website reliability, based on free and open source data and models.


# Limitations

- Veri.FYI currently gives the same output for different pages of the same website (with the exception of aggregating external links from a given URL).

- Veri.FYI currently does not analyze individual claims or content (text, images, audio, video).

- Veri.FYI is currently not intended for social media posts, except to give data to help assess reliability for the social media website/platform as a whole.

# Planned features

- Additional datasets for assessing source reliability
- Browser extension version of Veri.FYI

# Related lists of resources

The following is an incomplete and unendorsed list of lists of potentially helpful related resources.

1. Responsible Machine Learning
- [Awesome Machine Learning Interpretability](https://github.com/jphall663/awesome-machine-learning-interpretability)

2. Green Computing
- [Green Software](https://github.com/Green-Software-Foundation/awesome-green-software)

3. Open-source Intelligence (OSINT)
- [OSINT Collection](https://github.com/Ph055a/OSINT_Collection)
- [Awesome OSINT](https://github.com/jivoi/awesome-osint)

4. Fake News Detection
- [Awesome Fake News Detection](https://github.com/wangbing1416/Awesome-Fake-News-Detection)
- [A collection of FND, RD, FC and GTD](https://github.com/ShaoqLin/Awesome-FND-RD-FC-GTD)

5. Automated Fact-Checking
- [Automated Fact-Checking Resources](https://github.com/Cartus/Automated-Fact-Checking-Resources)

6. Deepfake Detection
- [Awesome Deepfakes Detection](https://github.com/Daisy-Zhang/Awesome-Deepfakes-Detection)
- [Awesome Comprehensive Deepfake Detection](https://github.com/qiqitao77/Awesome-Comprehensive-Deepfake-Detection)

7. Generative AI Content Detection
- [Awesome-AIGCDetection](https://fdmas.github.io/AIGCDetect/Awesome-AIGCDetection.html)
- [Awesome-AIGC-Detection](https://github.com/Daisy-Zhang/Awesome-AIGC-Detection/tree/main)

8. Data Journalism
- [Awesome data journalism](https://github.com/jwyg/awesome-data-journalism)
- [Awesome Data Journalism](https://github.com/infoculture/awesome-datajournalism)


# Related topics

- [Fact-checking](https://en.wikipedia.org/wiki/Fact-checking)
- [Fake news](https://en.wikipedia.org/wiki/Fake_news)
- [Disinformation](https://en.wikipedia.org/wiki/Disinformation)
- [Misinformation](https://en.wikipedia.org/wiki/Misinformation)
- [Fake news website](https://en.wikipedia.org/wiki/Fake_news_website)
- [Algorithmic bias](https://en.wikipedia.org/wiki/Algorithmic_bias)
- [Green computing](https://en.wikipedia.org/wiki/Green_computing)
- [Generative artificial intelligence](https://en.wikipedia.org/wiki/Generative_artificial_intelligence)
- [Open-source intelligence](https://en.wikipedia.org/wiki/Open-source_intelligence)
- [Media literacy](https://en.wikipedia.org/wiki/Media_literacy)
- [Data journalism](https://en.wikipedia.org/wiki/Data_journalism)
- [Open source](https://en.wikipedia.org/wiki/Open_source)
- [Wikipedia:Reliable sources](https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources)
