# Import Packages

## Installed Packages
import pytest

## Custom Libraries
import src.info_collection as ic

test_uses_url_shortener_params = [
    ("https://www.cnn.com", 0),
    ("https://bit.ly/abcdefg", 1),
]


@pytest.mark.parametrize("test_data, expected_output", test_uses_url_shortener_params)
def test_uses_url_shortener(test_data, expected_output):
    assert ic.uses_url_shortener(test_data) == expected_output


test_get_domain_params = [
    ("https://example.com", "example.com"),
    (
        "https://web.archive.org/web/20240101000832/https://www.example.com/",
        "example.com",
    ),
    ("https://bit.ly/48SH1y8", "espn.com"),
]


@pytest.mark.parametrize("test_data, expected_output", test_get_domain_params)
def test_get_domain(test_data, expected_output):
    assert ic.get_domain(test_data) == expected_output


test_sanitize_url_params = [
    ("http://example.com", "hxxp://example[.]com"),
    ("https://example.com", "hxxps://example[.]com"),
]


@pytest.mark.parametrize("test_data, expected_output", test_sanitize_url_params)
def test_sanitize_url(test_data, expected_output):
    assert ic.sanitize_url(test_data) == expected_output


test_input_validation_params = [
    ("https://example.com", ("https://example.com", "example.com")),
    ("example.com", ("https://example.com", "example.com")),
]


@pytest.mark.parametrize("test_data, expected_output", test_input_validation_params)
def test_input_validation(test_data, expected_output):
    assert ic.input_validation(test_data) == expected_output
