# Import Packages
## Base Packages
import unittest

## Installed Packages
import pytest

## Custom Libraries
from src.app import server


class TestAPI(unittest.TestCase):
    def setUp(self):
        self.app = server.test_client()

    def test_api(self):
        response = self.app.post("/assess", json={"url": "https://example.com"})
        assert response.json == {
            "Total Unreliable Source Blocklist Hits": 0,
            "Unreliable Source Blocklist Hits": [],
            "Total Spam Blocklist Hits": 0,
            "Spam Blocklist Hits": [],
        }
